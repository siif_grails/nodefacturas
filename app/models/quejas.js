var dbActions = require('../services/principal_db');
var config = require('../config/rest.js');
var fetch = require('node-fetch');

module.exports = {
	getListaQuejas: function(req, res) {
		dbActions.getListaQuejas(function(quejas) {
			res.json(quejas);
		});

	},

	insertarQueja: function(jsonQueja, res) {
		var _queja = dbActions.insertarQueja(jsonQueja, function(queja) {
			res.json(queja);
		});

    },

	updateQueja: function(jsonQueja, res) {
		var _queja = dbActions.updateQueja(jsonQueja, function(queja) {
			res.json(queja);
		});

    },
   

	asignaMensajeEvent: function(req, res) {
		//se asignan las variables al evento del menasje (correlacion de camunda)
console.log("**Timer disparado**");		
console.log(new Date());		
console.log(req.url);		

		let _name = req.query.name
		if (_name == "MensajeQueja") {

		 	fetch(config.urls.correlacionMensaje, 
		          {   method:"POST",
		              headers: {
		                'Accept': 'application/json',
		                'Content-Type': 'application/json'
		              },
		              body: JSON.stringify({ "messageName": _name,
		              						 "resultEnabled": true,
		              						 "businessKey": req.query.bKey,
		              						 "processVariables": {	"documentoCliente": 
		              	                     									{"type": "String",
		              	                     									 "value": req.query.doc,
		              	                     									 "valueInfo": {}
		              	                     									},
									                        		"nombreCliente": 	
									                        					{"type": "String",
									                        					 "value": req.query.nombre,
									                        					 "valueInfo": {}
									                       						}
		                      							  		}
		                 	})
		          }
		    )
		    .then(function(response) { 
		      return response.json()
		    })
		    .then(function(jsonObject) {
		      console.log("***********");
		      console.log("Resultado de la correlacion");
		      console.log(jsonObject)
		      res.json({quejaRegistrada: true});
		    })
		    .catch(function(err) {
		        console.log(err)
		    }); 

		} else if (_name == "MensajeFacturaElaborada") {

			console.log("Correlacionar "+_name);

		 	fetch(config.urls.correlacionMensaje, 
		          {   method:"POST",
		              headers: {
		                'Accept': 'application/json',
		                'Content-Type': 'application/json'
		              },
		              body: JSON.stringify({ "messageName": _name,
		              						 "resultEnabled": true,
		              						 "businessKey": req.query.bKey
		                 	})
		          }
		    )
		    .then(function(response) { 
		      return response.json()
		    })
		    .then(function(jsonObject) {
		      console.log("***********");
		      console.log("Resultado de la correlacion");
		      console.log(jsonObject)
		    })
		    .catch(function(err) {
		        console.log(err)
		    }); 
		}
	},

}