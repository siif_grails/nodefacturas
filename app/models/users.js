module.exports = {
	users: [
    { id: 1, 
      username: 'user1', 
      password: 'user1', 
      displayName: 'Jack', 
      email: 'jack@example.com',
      authorizations: [
      	"ver_facturas",
      	"ver_clientes",
      	"solicitar_factura",
      	"facturas_x_crear"
      ]
  	},
  	{ id: 2, 
  	  username: 'user2', 
  	  password: 'user2', 
  	  displayName: 'Jill', 
  	  email: 'jill@example.com',
      authorizations: [
      	"crear_factura",
      	"crear_clientes",
      	"clientes_x_crear"
      ]
	}
	],

	getUserByUserName: function(username, cb) {	
		console.log("en: findByUsername")		
	    for (var i = 0, len = this.users.length; i < len; i++) {
	      var record = this.users[i];
	      if (record.username == username) {
	        return cb(null, record);
	      }
	    }
	    return cb(null, null);
	},    

	getAutorizacionesUser: function(userId,cb) {	
		console.log("en: Autorizaciones")		
	    for (var i = 0, len = this.users.length; i < len; i++) {
	      var record = this.users[i];
	      if (record.id == userId) {
	      	console.log("record.authorizations")
	      	console.log(record.authorizations)
        	return cb(record.authorizations);
	      }
	    }
	    /*no encontro el usuario*/
	    return cb(null);
	}    

}