var dbActions = require('../services/principal_db');

module.exports = {
	getListaClientes: function(req, res, id) {
		dbActions.getListaClientes(function(factura) {
			res.json(factura);
		});

	},

	getFactura: function(req, res, id) {
		dbActions.getFactura(id, function(factura) {
			res.json(factura);
		});

	},

	getDetallesFactura: function(req, res, id) {
		dbActions.getDetallesFactura(id, function(detalles) {
			res.json(detalles);
		});
	},

	getListaFacturas: function(req, res) {
		dbActions.getListaFacturas(function(facturas) {
			res.json(facturas);
		});

	},

	insertarFactura: function(jsonFactura, res) {
		var _factura = dbActions.insertarFactura(jsonFactura, function(factura) {
			res.json(factura);
		});

    },

    updateFactura: function(jsonFactura, res, id) {
		var _factura = dbActions.updateFactura(jsonFactura, id, function(factura) {
			res.json(factura);
		});

    },

    updateDetalleFactura: function(jsonDetalle, res, id) {
		var _detalle = dbActions.updateDetalleFactura(jsonDetalle, id, function(detalle) {
			res.json(detalle);
		});

    },

	insertarCliente: function(jsonCliente, res) {
		var _cliente = dbActions.insertarCliente(jsonCliente, function(cliente) {
			res.json(cliente);
		});

    },

	verificaCliente: function(req, res) {
		dbActions.verificaCliente(req, function(cliente) {
			res.json(cliente);
		});

	},
    updateCliente: function(jsonCliente, res, id) {
		var _cliente = dbActions.updateCliente(jsonCliente, id, function(cliente) {
			res.json(cliente);
		});

    },
	verificaCartera: function(req, res) {
		dbActions.verificaCartera(req, function(cartera) {
			res.json(cartera);
		});

	},    
}