'use strict';
const nodemailer = require('nodemailer');


module.exports = {

    sendMail: function(mailData){

        /*
        let mailOptions = {
            from: '"Fred Foo 👻" <foo@blurdybloop.com>', // sender address
            to: 'bar@blurdybloop.com, baz@blurdybloop.com', // list of receivers
            subject: 'Hello ✔', // Subject line
            text: 'Hello world ?', // plain text body
            html: '<b>Hello world ?</b>' // html body
        };
        */

        let mailOptions = mailData;

        // create reusable transporter object using the default SMTP transport
        let transporter = nodemailer.createTransport({
            host: 'mail.siifweb.com',
            port: 25,
            secure: false, // secure:true for port 465, secure:false for port 587
            tls: {
                    rejectUnauthorized: false
                },
            auth: {
                user: 'gestiondoc@siifweb.com',
                pass: 'G3st10nD0c2020'
            }
        });

        // send mail with defined transport object
        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                return console.log(error);
            }
            console.log('Message %s sent: %s', info.messageId, info.response);
        });
    }

}