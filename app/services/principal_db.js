var mysqlService  = require('./mysql_service');
var mongoService  = require('./mongo_service');

var db = process.argv[2];
if (db == undefined || db == "")
	db = "mongo";

var service = db == "mysql" ? mysqlService : mongoService;

module.exports = {

	getListaClientes: function(cbFunction) {
		service.getListaClientes(cbFunction);
	},

	getFactura: function(id, cbFunction) {
		service.getFactura(id, cbFunction);
	},

	getListaFacturas: function(cbFunction) {
		service.getListaFacturas(cbFunction);
	},

	getListaQuejas: function(cbFunction) {
		service.getListaQuejas(cbFunction);
	},

	getDetallesFactura: function(id, cbFunction) {
		service.getDetallesFactura(id, cbFunction);
	},

	insertarFactura: function(jsonFactura, cbFunction) {
		var factura = service.insertarFactura(jsonFactura, cbFunction);
	},

	insertarQueja: function(jsonQueja, cbFunction) {
		var queja = service.insertarQueja(jsonQueja, cbFunction);
	},

	updateFactura: function(jsonFactura, id, cbFunction) {
		var factura = service.updateFactura(jsonFactura, id, cbFunction);
	},

	updateQueja: function(jsonQueja, cbFunction) {
		var queja = service.updateQueja(jsonQueja.idQueja, cbFunction);
	},

	updateDetalleFactura: function(jsonDetalle, id, cbFunction) {
		var detalle = service.updateDetalleFactura(jsonDetalle, id, cbFunction);
	},

	insertarCliente: function(jsonCliente, cbFunction) {
		var cliente = service.insertarCliente(jsonCliente, cbFunction);
	},

	verificaCliente: function(req, cbFunction) {
		service.verificaCliente(req, cbFunction);
	},
	updateCliente: function(jsonCliente, id, cbFunction) {
		var cliente = service.updateCliente(jsonCliente, id, cbFunction);
	},	

	verificaCartera: function(req, cbFunction) {
		service.verificaCartera(req, cbFunction);
	},
}


