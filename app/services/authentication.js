const jwt = require('jsonwebtoken');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var JsonStrategy = require('passport-json').Strategy;
var users = require('../models/users');

passport.use(new JsonStrategy(
  function(username, password, cb) {
    console.log("en json strategy");    
    users.getUserByUserName(username, function(err, user) {
        console.log("en el cb de json strategy");

        if (err) { 
            console.log("error in json stratgey function");
            console.log(err);        
            return cb(err, false, {msg: err}); 
        }
        if (!user) { 
            return cb(null, false, {msg: "No se encontró el usuario"}); 
        }
        if (user.password != password) { 
            return cb(null, false, {msg: "Password no es pero el usuario ok. Por ahí es"}); 
        }
        /*trae las atorizaciones del usuario*/
        users.getAutorizacionesUser(user.id, function(autorizaciones){
console.log("xxxx")            
console.log(autorizaciones)            
            return cb(null, user, {msg: "okx"}, "x-xxyyy");    
        });
    });
  })
);



module.exports = {

    initializePassport: function(app) {
        app.use(passport.initialize());
        app.use(passport.session());
    },

    login : function(req,res,next) {
        console.log("**** Ahora procesado por el routerx *****");
        var _token = null;
        var z = passport.authenticate('json', function(err, user, info, autorizaciones) {
            console.log("en el cb de del router autorizaciones");
            console.log(autorizaciones)            
            console.log(info)            
            console.log({err:err, user:user, info:info});            
            var _jsonObject= {ok:false, token:false, msg: info.msg};

            if (!err && user && user.id) {
                /*Generar el token*/
                _token = jwt.sign({id: user.id}, 'server secret', {expiresIn: 30});
                _jsonObject = {ok:true, token:_token, msg: info.msg, autorizaciones: autorizaciones};
            }
            res.json(_jsonObject);
            console.log(_token);
        })(req, res, next);

    },

    verifyToken: function(req, res) {
        //for all api endpoints excepto login
        var _token;
        if (req.headers.authorization) {
            _token = req.headers.authorization.replace("Bearer ","");
            jwt.verify(_token, 'server secret', function(err, decoded) {
                if (err) {
                    console.log("error token")
                }
                else{
                    //token valido
                    console.log("token válido")
                }
            });
        }   
    },

}
