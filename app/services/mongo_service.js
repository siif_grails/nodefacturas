var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectId;
var assert = require('assert');
var moment = require('moment');

function fnFacturaMap(factura){

	//ID de la factura para componente REST
	factura.id = factura._id;

	var _total = 0;
	if (factura.items){
		factura.items.forEach(function(item){
			_total = _total + (item.cantidad * item.valor);
		});
	}

	factura.valor = _total;
	return factura;

}

function fnItemFacturaMap(item, idFactura, index){

	//ID del  item de la factura para componente REST
	item.id = idFactura+'_'+index;
	return item;

}

function fnClienteMap(cliente){

	//ID del cliente para componente REST
	cliente.id = cliente._id;
	return cliente;

}

function fnQuejaMap(queja){

	//ID de la queja para componente REST
	queja.idQueja = queja._id;
	return queja;

}

module.exports = {

	mongoCommand: function(dbExecute) {
		var url = 'mongodb://localhost:27017/siif';
		MongoClient.connect(url, function(err, db) {
		  assert.equal(null, err);
		  console.log("Connected correctly to siif.");
		  dbExecute(db);
		});
	},

	endConnection: function(db) {
		db.close();
	},

	getCliente: function(id, cbFunction) {
		var _this = this;
		this.mongoCommand(function(db){
				db.collection("clientes").findOne({_id: new ObjectId(id)}, function(err, result){
					_this.endConnection(db);
					cbFunction(fnClienteMap(result));
				});
		});
	},

	getListaClientes: function(cbFunction) {
		var _this = this;
		this.mongoCommand(function(db){
				db.collection("clientes").find({}).map(function(cliente){return fnClienteMap(cliente);}).toArray(function(err, result){
					_this.endConnection(db);
					cbFunction(result);
				});
		});
	},

	getQueja: function(id, cbFunction) {
		var _this = this;
		this.mongoCommand(function(db){
				db.collection("quejas").findOne({_id: new ObjectId(id)}, function(err, result){
					_this.endConnection(db);
					cbFunction(fnQuejaMap(result));
				});
		});
	},

	getFactura: function(id, cbFunction) {
		var _this = this;
		this.mongoCommand(function(db){
				db.collection("facturas").findOne({_id: new ObjectId(id)}, function(err, result){
					_this.endConnection(db);
					cbFunction(fnFacturaMap(result));
				});
		});
	},

	getListaQuejas: function(cbFunction) {
		var _this = this;
		this.mongoCommand(function(db){
				db.collection("quejas").find({}).sort({dateCreated: -1}).map(function(queja){return fnQuejaMap(queja);}).toArray(function(err, result){
					_this.endConnection(db);
					console.log("result");
					//console.log(result);				
					cbFunction(result);
				});
		});
	},


	getListaFacturas: function(cbFunction) {
		var _this = this;
		this.mongoCommand(function(db){
				db.collection("facturas").find({}).sort({dateCreated: -1}).map(function(factura){return fnFacturaMap(factura);}).toArray(function(err, result){
					_this.endConnection(db);
					console.log("result");
					//console.log(result);				
					cbFunction(result);
				});
		});
	},

	getDetallesFactura: function(id, cbFunction) {
		var _this = this;
		this.mongoCommand(function(db){
				db.collection("facturas").findOne({_id: new ObjectId(id)}, function(err, result){
					_this.endConnection(db);
					var _items = result.items.map(function(item, index){
						return fnItemFacturaMap(item, id, index);
					});
					cbFunction(_items);
				});
		});
	},

	getDetalleFactura: function(id, cbFunction) {
		var _this = this;
		var _aId = id.split("_");
		var _factura_id = _aId[0];
		var _item_index = _aId[1];

		this.mongoCommand(function(db){
				db.collection("facturas").findOne({_id: new ObjectId(_factura_id)}, function(err, result){
					_this.endConnection(db);
					var _item = fnItemFacturaMap(result.items[_item_index], _factura_id, _item_index);
					cbFunction(_item);
				});
		});
	},

	insertarQueja: function(jsonQueja, cbFunction) {
		var _this = this;

		/* clona el objecto json para evitar copiarlo por referencia*/
		var _queja = JSON.parse(jsonQueja);

		_queja.fecha = new Date();

		console.log(jsonQueja);
		
		if (jsonQueja.dateCreated == undefined || jsonQueja.dateCreated == null || jsonQueja.dateCreated == "")
			_queja.dateCreated = new Date();
		if (jsonQueja.lastUpdated == undefined || jsonQueja.lastUpdated == null || jsonQueja.lastUpdated == "")
			_queja.lastUpdated = new Date();

console.log("_queja");
console.log(_queja);
		//Busca el cliente de la factura por número de documento
		this.mongoCommand(function(db){

						_this.mongoCommand(function(db){
							db.collection("quejas").insertOne(_queja, function(err, result){
								_this.endConnection(db);
								var _idQueja = result.insertedId;
			                    _this.getQueja(_idQueja, cbFunction);
							});
					});

				

		});

	},

	insertarFactura: function(jsonFactura, cbFunction) {
		var _this = this;

		/* clona el objecto json para evitar copiarlo por referencia*/
		var _factura = JSON.parse(JSON.stringify(jsonFactura));

		//Si la fecha llega vacia, se usa la fecha actual del servidor
		if (jsonFactura.fecha == undefined || jsonFactura.fecha == null || jsonFactura.fecha == "")
			_factura.fecha = new Date();
		//Si llega una cadena con la fecha, se utiliza moment.js para parsear la fecha
		else
			_factura.fecha = moment(jsonFactura.fecha).toISOString();
		
		if (jsonFactura.dateCreated == undefined || jsonFactura.dateCreated == null || jsonFactura.dateCreated == "")
			_factura.dateCreated = new Date();
		if (jsonFactura.lastUpdated == undefined || jsonFactura.lastUpdated == null || jsonFactura.lastUpdated == "")
			_factura.lastUpdated = new Date();

		//Busca el cliente de la factura por número de documento
		this.mongoCommand(function(db){

				db.collection("clientes").findOne({documento: _factura.documento}, function(err, result){
					_this.endConnection(db);

					// Asigna el cliente encontrado
					_factura.cliente = result;

					delete _factura["documento"];
					delete _factura["nombre"];

					_this.mongoCommand(function(db){
							db.collection("facturas").insertOne(_factura, function(err, result){
								_this.endConnection(db);
								var _idFactura = result.insertedId;
			                    _this.getFactura(_idFactura, cbFunction);
							});
					});

				});

		});

	},

	updateQueja: function(idQueja, cbFunction) {
		var _this = this;

		console.log("Datos queja update");
		console.log(idQueja);

		//Busca el cliente de la factura por número de documento
		this.mongoCommand(function(db){

			db.collection("quejas").update({idQueja: idQueja}, {$set: {observacion:'Factura generada'}}, function(err, result){
				_this.endConnection(db);


            cbFunction(result)

			});

		});

	},

	updateFactura: function(jsonFactura, id, cbFunction) {
		var _this = this;

		console.log("Datos factura update");
		console.log(jsonFactura);

		//Busca el cliente de la factura por número de documento
		this.mongoCommand(function(db){

			db.collection("facturas").findOne({_id: new ObjectId(id)}, function(err, result){
				_this.endConnection(db);
				_factura = result;

				//Actualización de datos enviados por el formulario
				_factura.numero = jsonFactura.numero; 
				if (jsonFactura.fecha != undefined && jsonFactura.fecha != null && jsonFactura.fecha != "")
					_factura.fecha = moment(jsonFactura.fecha, "YYYY-MM-DD").toISOString();

				//Busca el cliente de la factura por número de documento
				_this.mongoCommand(function(db){

						db.collection("clientes").findOne({documento: jsonFactura.documento}, function(err, result){
							_this.endConnection(db);

							// Asigna el cliente encontrado
							_factura.cliente = result;

							_this.mongoCommand(function(db){
									db.collection("facturas").save(_factura, function(err, result){
										_this.endConnection(db);
					                    _this.getFactura(id, cbFunction);
									});
							});

						});

				});


			});

		});

	},

	updateDetalleFactura: function(jsonDetalle, id, cbFunction) {
		var _this = this;
		var _aId = id.split("_");
		var _factura_id = _aId[0];
		var _item_index = _aId[1];

		this.mongoCommand(function(db){
				db.collection("facturas").findOne({_id: new ObjectId(_factura_id)}, function(err, result){
					_this.endConnection(db);

					var _item = result.items[_item_index];
					_item.item = jsonDetalle.item;
					_item.cantidad = jsonDetalle.cantidad;
					_item.valor = jsonDetalle.valor;
					_item.lastUpdated = new Date();
					//result.items[_item_index] = _item;

					_this.mongoCommand(function(db){
							db.collection("facturas").save(result, function(err, result){
								_this.endConnection(db);
								_this.getDetalleFactura(id, cbFunction);
							});
					});

				});
		});
	},

	insertarCliente: function(jsonCliente, cbFunction) {
		var _this = this;

		/* clona el objecto json para evitar copiarlo por referencia*/
		var _cliente = JSON.parse(JSON.stringify(jsonCliente));
		_cliente.dateCreated = new Date();
		_cliente.lastUpdated = new Date();

		_this.mongoCommand(function(db){
				db.collection("clientes").insertOne(_cliente, function(err, result){
					_this.endConnection(db);
					var _idCliente = result.insertedId;
                    _this.getCliente(_idCliente, cbFunction);
				});
		});

	},

	verificaCliente: function(req, cbFunction) {
		var _this = this;
		//console.log(req);
		this.mongoCommand(function(db){
				db.collection("clientes").findOne({documento: req.query.documentoCliente}, function(err, result){
					_this.endConnection(db);
					var _cliente = JSON.parse(JSON.stringify(result));
					if (result) {
						_cliente.existe = true;
					} else {
						_cliente = {existe: false};
					}
					cbFunction(_cliente);
				});
		});
	}
	,
	updateCliente: function(jsonCliente, id, cbFunction) {
		var _this = this;

		console.log("Datos Cliente update");
		console.log(jsonCliente);

		this.mongoCommand(function(db){
				db.collection("clientes").findOne({_id: new ObjectId(id)}, function(err, result){
				_this.endConnection(db);
				_cliente = result;

				if (jsonCliente.documento != undefined && jsonCliente.documento != null && jsonCliente.documento != "")
					_cliente.documento = jsonCliente.documento;
				if (jsonCliente.nombre != undefined && jsonCliente.nombre != null && jsonCliente.nombre != "")
					_cliente.nombre = jsonCliente.nombre;
				if (jsonCliente.telefono != undefined && jsonCliente.telefono != null && jsonCliente.telefono != "")
					_cliente.telefono = jsonCliente.telefono;
				if (jsonCliente.email != undefined && jsonCliente.email != null && jsonCliente.email != "")
					_cliente.email = jsonCliente.email;
				if (jsonCliente.direccion != undefined && jsonCliente.direccion != null && jsonCliente.direccionl != "")
					_cliente.direccion = jsonCliente.direccion;
	
					_this.mongoCommand(function(db){
									db.collection("clientes").save(result, function(err, result){
										_this.endConnection(db);
					                    _this.getCliente(id, cbFunction);
									});
					});
				});

		});

	},


	verificaCartera: function(req, cbFunction) {
		var _this = this;
		this.mongoCommand(function(db){
	        db.collection('facturas').aggregate(
	        [
				{ $match: { "cliente.documento": req.query.documentoCliente} },
				{ $project: { "documento": "$cliente.documento", items: 1, _id: 0 } },
				{ $unwind: "$items" },
				{ $project: { documento: 1, total: { $multiply: [ "$items.valor", "$items.cantidad" ] } } },
				{ $group: { 
					_id: "$documento",
					total: { $sum: "$total" } } 
				} 
	        ], function(err, result){
	            if (!err) {
					_this.endConnection(db);
					
					cbFunction({valorCartera: (result && result.length > 0 ? result[0].total : 0)});
	            }
	        });
    	});
    }	

}