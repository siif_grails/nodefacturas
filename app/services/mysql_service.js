var mysql      = require('mysql');

module.exports = {

	initConnection: function() {
		var connection = mysql.createConnection({
		  host     : 'localhost',
		  user     : 'siif',
		  password : 'siif',
		  database : 'siif_schema'
		});
		connection.connect();
		return(connection);
	},
	endConnection: function(connection) {
		connection.end();
	},

	getFactura: function(id, cbFunction) {
		var _connection = this.initConnection();
		_this = this;
		_connection.query('SELECT * from encabezado_factura where id=' + id, function (error, results, fields) {
			_this.endConnection(_connection);
			cbFunction(results);
		});
	},

	getListaFacturas: function(cbFunction) {
		var _connection = this.initConnection();
		_this = this;
		_connection.query('SELECT * from encabezado_factura', function (error, results, fields) {
			_this.endConnection(_connection);
			cbFunction(results);
		});
	},

}