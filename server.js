var express = require('express');
var app = express();
var cors = require('cors');
var bodyParser = require('body-parser')

var facturas = require('./app/models/facturas');
var emailService = require('./app/services/emailService');
var authentication = require('./app/services/authentication');

var eureka = require('eureka-js-client').Eureka;

var quejas = require('./app/models/quejas');

var router = express.Router();    // get an instance of the express Router

// middleware to use for all requests
router.use(function(req, res, next) {
    // do logging
    console.log('Peticion url realizada.');
    console.log(req.url);
    //veritifca token si no está en login
    if (req.url != "/login") {
        authentication.verifyToken(req,res);    
    }
    next(); // make sure we go to the next routes and don't stop here
});

var use_db = "mysql";

router.get('/', function(req, res) {
    res.json({ message: 'Se invocó el url raíz' }); 
});

router.route("/cliente/verificarCartera")
    .get(function(req, res) {
        facturas.verificaCartera(req, res);
    });
    
router.route("/cliente")
    .get(function(req, res) {
        facturas.getListaClientes(req, res);
    })
    .post(function(req, res) {
        facturas.insertarCliente(req.body, res);
    });
router.route("/cliente/:id")
    .get(function(req, res) {
        facturas.getCliente(req, res, req.params.id);
    })
    .patch(function(req, res) {
        facturas.updateCliente(req.body, res, req.params.id);
    });    

router.route("/verificarCliente")
    .get(function(req, res) {
        facturas.verificaCliente(req, res);
    });

router.route("/queja")
    .get(function(req, res) {
        quejas.getListaQuejas(req, res);
    })
    .post(function(req, res) {

        var body = '';
        req.on('data', function (data) {
                body += data;
        });
        req.on('end', function () {
                quejas.insertarQueja(body, res);
        });
    })
    .put(function(req, res) {
        quejas.updateQueja(req.body, res);
    });

router.route("/facturas")
    .get(function(req, res) {
        facturas.getListaFacturas(req, res);
    })
    .post(function(req, res) {
        facturas.insertarFactura(req.body, res);
    });

router.route("/facturas/:id")
    .get(function(req, res) {
        facturas.getFactura(req, res, req.params.id);
    })
    .patch(function(req, res) {
        facturas.updateFactura(req.body, res, req.params.id);
    });

router.route("/facturas/:id/items")
    .get(function(req, res) {
        facturas.getDetallesFactura(req, res, req.params.id);
    });

router.route("/items/:id")
    .put(function(req, res) {
        facturas.updateDetalleFactura(req.body, res, req.params.id);
    });

router.route("/getRoles")
    .get(function(req, res) {
        res.json({roles: "cartera, administrativo"});
    });

router.route("/enviarMail")
    .get(function(req, res) {

        console.log("mail "+req.query.email);
        console.log("valorCartera "+req.query.valorCartera);

        let mailOptions = {
            from: '"Edison Giraldo" <gestiondoc@siifweb.com>', // sender address
            to: req.query.email, // list of receivers
            subject: 'Solicitud de factura RECHAZADA', // Subject line
            //text: 'Su solicitud de generación de factura ha sido rechazada. Se encontró cartera por valor de '+req.valorCartera, // plain text body
            html: 'Su solicitud de generación de factura ha sido rechazada. Se encontró cartera por valor de <b>'+req.query.valorCartera+'</b>' // html body
        };

        emailService.sendMail(mailOptions);

        res.json({estado: "OK"});

    });


router.route("/queja/asignarMensajeEvent")
    .get(function(req, res) {
        console.log("name "+req.query.name);
        console.log("doc "+req.query.doc);
        console.log("nombre Cliente "+req.query.nombre);

        quejas.asignaMensajeEvent(req, res);

    });    

router.route('/login')
    .post(function(req, res, next) {
        authentication.login(req, res, next);
    });
  
router.all('*', cors());

// parse application/json 
app.use(bodyParser.json());

router.all('*', cors());
app.use(cors());
app.use('/', router);

//inicializa passport
authentication.initializePassport(app);


const client = new eureka({
  // application instance information 
  instance: {
    app: 'nodefacturas',
    instanceId: 'nodef42',
    hostName: 'localhost',
    ipAddr: '127.0.0.1',
    statusPageUrl: 'http://localhost:3000',
    port: {
      '$': 3000,
      '@enabled': true,
    },    
    vipAddress: 'nodefacturas.com',
    dataCenterInfo: {
      '@class': 'com.netflix.appinfo.InstanceInfo$DefaultDataCenterInfo',
      name: 'MyOwn',
    },
  },
  eureka: {
    // eureka server host / port 
    host: '127.0.0.1',
    port: 8761,
    servicePath: '/eureka/apps'
  },
});

//inicia el listener
app.listen(3000, function () {
  console.log('Example app listening on port 3000!')
  // client.start();
})

